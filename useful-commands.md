These commands are used in the Deploy to EKS from Jenkins lecture

Install kubectl on Jenkins server

`curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl; chmod +x ./kubectl; mv ./kubectl /usr/local/bin/kubectl`



Install aws-iam-authenticator on Jenkins server

`curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.15.10/2020-02-22/bin/linux/amd64/aws-iam-authenticator`

`chmod +x ./aws-iam-authenticator`

`mv ./aws-iam-authenticator /usr/local/bin`




Copy config file to Jenkins server

`docker cp config "YOUR DOCKER CONTAINER ID":/var/jenkins_home/.kube/`


If you have re-deployed your jenkins container and docker is not available on it then run these inside container

`curl https://get.docker.com/ > dockerinstall && chmod 777 dockerinstall && ./dockerinstall`
